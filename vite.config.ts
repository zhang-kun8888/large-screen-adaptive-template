import { defineConfig } from "vite";
import AutoImport from "unplugin-auto-import/vite";
import Components from "unplugin-vue-components/vite";
import vue from "@vitejs/plugin-vue";
import { ElementPlusResolver } from "unplugin-vue-components/resolvers";
import path from "path";

export default defineConfig({
  base: "./",
  plugins: [
    vue(), //vue
    AutoImport({
      //引入vue 自动注册api插件
      imports: ["vue"],
      dts: "src/auto-import.d.ts",
    }),
    AutoImport({
      //plus按需引入
      // resolvers: [ElementPlusResolver()],
    }),
    Components({
      //plus按需引入
      // resolvers: [ElementPlusResolver()],
    }),
  ],
  build: {
    minify: "terser",
    terserOptions: {
      compress: {
        //生产环境时移除console
        drop_console: true,
        drop_debugger: true,
      },
    },
  },

  resolve: {
    //配置根路径别名： import('@/pages/login/login.vue')
    alias: {
      "@": path.resolve(__dirname, "src"),
    },
  },
  // 跨域
  server: {
    //使用IP能访问
    host: "0.0.0.0",
    // 热更新
    hmr: true,
    //自定义代理规则
    proxy: {
      // 选项写法
      "/api": {
        target: "https://admin.itrustnow.com",
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, ""),
      },
    },
  },
});
