# 大屏自适应 --- 模板

> Vue 3 + TypeScript + Vite

# 包含什么？

- 此项目可直接运行 集成了vuex router 封装axios + loading + 返回顶部 + 模块化vuex + 模块化路由

# 博文地址
- https://blog.csdn.net/m0_57904695/article/details/126985375?spm=1001.2014.3001.5501

# 图例：
![输入图片说明](https://img-blog.csdnimg.cn/9c204ad790ca473eb277c1e34832686a.gif)

# 安装

- yarn

# 运行

- yarn dev

# 发布

- yarn build

# 发布到 GitHub Pages

- yarn gh-pages

# 发布到 NPM

- yarn publish

# 发布到 NPM 私有仓库

- yarn publish --registry=https://registry.npm.taobao.org

# 发布到 NPM 私有仓库，并使用

- yarn publish --registry=https://registry.npm.taobao.org --access=public
